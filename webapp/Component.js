sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"rimmy/kakao/map/model/models"
], function (UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("rimmy.kakao.map.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
		},
		
		//여기에 firebase 이니셜 하는게 좋다,,,,,, 어떻게 하지,,,,?
		// firebaseInitioniztion : function(){
			
			
			
		// },
		
		// _initFirebase: function(){ debugger;
		// 	var firebaseConfig = {
		// 	    apiKey: "AIzaSyDOFcRs9ypLOBkSoRsSL4fNQeWKZeFaXtU",
		// 	    authDomain: "john-martang.firebaseapp.com",
		// 	    databaseURL: "https://john-martang.firebaseio.com",
		// 	    projectId: "john-martang",
		// 	    storageBucket: "john-martang.appspot.com",
		// 	    messagingSenderId: "227075007155",
		// 	    appId: "1:227075007155:web:295b27c779fa406a2e46c6",
		// 	    measurementId: "G-WCHTPMQQ4D"
		// 	  };
			
		// 	// Initialize Firebase
		// 	firebase.initializeApp(firebaseConfig);
		// 	//firebase.initializeApp({"uri" : "configs/firebase/init.js"});
		// 	this._storage = firebase.storage();
			
		// }
		
		
	});
});