sap.ui.define([
	// "sap/ui/core/mvc/Controller",
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	'sap/ui/core/Fragment'
], function (BaseController, JSONModel, MessageBox, Fragment) {
	"use strict";

	return BaseController.extend("rimmy.kakao.map.controller.App", {
	});
});