sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment"
], function (BaseController, JSONModel, MessageBox, Fragment ) {
	"use strict";
	return BaseController.extend("rimmy.kakao.map.controller.Create", {
		onInit: function () {
			this.router = this.getRouter();
			this.router.getRoute("Create").attachPatternMatched(this._onCreateMatched, this);
		},
		
		_onCreateMatched : function() {
			this.resetCreateData();
			this.createPageMakeMap(127.11674377596071 , 37.484177185974836, false, null);
			
			this.getView().setModel(new JSONModel({
				"rating" : 3,
				"date" : this.getTodayDate(),
				"fileArr" : []
			}), "createData");
			var originCreateData = JSON.parse(JSON.stringify(this.getView().getModel("createData").getData()));
			this.getView().setModel(new JSONModel(originCreateData), "originCreateData");
		},
		
		onAfterRendering : function(){
			this._getCreateSelectboxData();
		},
		
		/* 생성화면 관련 */
		_getCreateSelectboxData : function(){
			var that = this;
			$.ajax({
				url : "/map_rimmy/api/v1/main/code",
				method : "GET",
				async : false,
				success : function(data){ 
					that.getView().setModel(new JSONModel(data.region), "region"); 
					that.getView().setModel(new JSONModel(data.foodkind), "foodkind"); 
				},
				error : function(error){
					MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
				}
			});
		},
		
		
		
		createPageMakeMap : function(x,y,draggable,pName){
			var position  = new kakao.maps.LatLng(y, x); 
			this.getView().byId("mapCreateInfo").setText(pName);
			
			if(!this.createMap){
				var divId = this.getView().byId("mapCreateContent").getId();
				var container = document.getElementById(divId);
				
				var infoDivId = this.getView().byId("mapCreateInfo").getId();
				var infoContainer = document.getElementById(infoDivId);
				
				var options = {
					center: position,
					level: 3 
				};
			
				this.createMap = new kakao.maps.Map(container, options);
				this.createMap.setDraggable(draggable);
				this.createMarker = new kakao.maps.Marker({
					map : this.createMap,
				    position: position
				});
				this.createMarker.setMap(this.createMap);
				this.createInfowindow = new kakao.maps.InfoWindow({
				    position : position, 
				    content : infoContainer
				});
				if(pName){
					this.createInfowindow.open(this.createMap, this.createMarker); 
				}
			}else{
			    this.createMap.setCenter(position);
			    this.createMap.panTo(position);    
			    this.createMarker.setPosition(position);
			    if(pName){
			    	this.createInfowindow.open(this.createMap, this.createMarker);
			    	this.createInfowindow.setPosition(position);
			    }
			}
		},
				
		onRequiredChange : function(event){
			var createData = this.getView().getModel("createData").getData();
			var selectedPrice = createData.priceSlider;
			createData.price = selectedPrice;
			
			//날짜 체크
			if(event){
				if(event.getSource().getId().split("-").reverse()[0] === "priceValue"){
					if( event.getSource().getValue() >= 0 &&  event.getSource().getValue() <= 100000){
						event.getSource().setValueState("None");
						event.getSource().setValueStateText("");
						createData.price = event.getSource().getValue().toLocaleString();
						createData.priceSlider = Number(event.getSource().getValue());
					}else{
						event.getSource().setValueState("Error");
						event.getSource().setValueStateText("0이상 100000이하의 값으로 입력해주세요.");
						event.getSource().setValue("");
						createData.priceSlider = 0;
					}
						
				}
				else if(event.getSource().getId().split("-").reverse()[0] === "ateDate"){
					var ateDate = event.getParameter("valid") === false || !event.getParameter("newValue") ?  false : true ;
				
					if(!ateDate){
						event.getSource().setValueState("Error");
						event.getSource().setValueStateText("ex: 20200130 과 같은 형식으로 입력해주세요.");
						event.getSource().setValue("");
						event.getSource().setPlaceholder("ex: 20200130");
					}else{
						event.getSource().setValueState("None");
						event.getSource().setValueStateText("");
					}
				}
			}
			//필수값 체크 
			if(createData.placeName){
				var placeName = createData.placeName.replace(/ /gi, "");
			}
			if(createData.address){
				var newAddr = createData.address.replace(/ /gi, "");
			}
			var isPriceValue = createData.price > 0 ? true : false ;
			var isAteDateValue = this.getView().byId("ateDate").getValueState() === "None" ? true : false;
			
			//조건들을 다 통과하면 저장버튼 활성화
			if( placeName && newAddr && isPriceValue && createData.rating && createData.address && isAteDateValue){
				this.getView().byId("idSave").setEnabled(true);
			}else{
				this.getView().byId("idSave").setEnabled(false);
			}
		},
		
		onSelectChange : function(event){
			var createData = this.getView().getModel("createData").getData();
			var selectedName = event.getParameter("selectedItem").getText();
			//카테고리가 클릭되면
			if(event.getParameter("id").split("-").reverse()[0] === "foodkind"){
				createData.foodkindName = selectedName;
			}else{
				createData.regionName = selectedName;
			}
		},
		
		/**
		*
		*파일 관련 
		*
		*/
		handleTypeMissmatch : function(event){
			// var fileType = event.getSource().getFileType();
			MessageBox.error("jpg, png 파일형식만 업로드 가능합니다.");
			
		},
		
		//파일 리스트 
		onAttachFile : function(event){
			var that = this;
			var oModel = that.getView().getModel("createData");
			var fileArr = oModel.getData().fileArr;
			
			if(fileArr.length>= 3){
				MessageBox.confirm("이미지 첨부는 최대 3개까지 가능합니다.",{
					title: "확인"
				});
				event.getSource().setValue("");
				return;
			}
			
			var fileObject = event.getParameter("files")[0]; //하나씩 가져오니까
			
			var FILESIZE = 1024 * 1024 * 3 ;
			if(fileObject.size > FILESIZE){
				event.getSource().setValue("");
				MessageBox.alert("파일 용량은 최대 3MB 입니다.");
				return;
			}
			
			if(fileObject){
				fileArr.push(fileObject);
				oModel.refresh();
				
				event.getSource().setValue("");
			}
			
			
		},
		
		onFileDelete : function(event){
			// debugger;
			var deletePath = event.getParameter("listItem").getBindingContextPath().split("/").reverse()[0];
			var oModel = this.getView().getModel("createData");
			var fileArr = oModel.getData().fileArr;
			
			fileArr.splice(deletePath,1);
			oModel.setData(oModel.getData());
			oModel.refresh();
		},
		
		onCreateSave : function(event){
			var that = this;
			var fileArr = this.getView().getModel("createData").getData().fileArr;
			
			MessageBox.confirm("저장하시겠습니까?", {
				title : "저장",
				onClose : function(action){
					if(action === "OK"){
						that.getView().byId("createPage").setBusy(true);
						if(fileArr.length > 0){
							that._saveFileStorage(fileArr);
						}else{
							that._saveCreateData();
						}
					}// OK END
				}
			});  //MessageBox END
		},
		
		_saveCreateData : function(fileData){
			var that = this;
			var createData = this.getView().getModel("createData").getData();
			
			//파일 처리
			if(fileData){
				createData.fileArr = JSON.parse(fileData).data;
			}else{
				if(!createData.fileArr){ createData.fileArr = []; }
			}
			
			//데이터 보수
			createData.price = createData.price.toString();
			createData.date = createData.date +  this.getCurrentTime();
			createData.delFlag = "";
			if(!createData.region){ createData.region = ""; createData.regionName = "";}
			if(!createData.foodkind){ createData.foodkind = ""; }
			if(!createData.recoMan){ createData.recoMan = ""; }
			if(!createData.comment){ createData.comment = ""; }
			delete createData.priceSlider;
			
			//데이터 전송
			$.ajax({
				url :  "/map_rimmy/api/v1/martang",
				method : "POST",
				headers: {
					"Content-Type": "application/json; charset=utf-8"
				},
				data: JSON.stringify(createData),
				success : function(data,headers){ 
					if(data.status === "Success"){
						MessageBox.success(data.msg, {
						    title: "확인",
						    onClose: function(action){
						    	if(action==="OK"){
						    		that.reload = true;
						    		that.getOwnerComponent().reload = true;
						    		that.router.navTo("Main"); 
						    	}
						    }
						});
						that.getView().byId("createPage").setBusy(false);
						// that.resetCreateData();
					}
				},
				error : function(error){
					//이미 업로드된 파일 삭제
					if(createData.fileArr){
						createData.fileArr.forEach(function(file){
							var httpsReference = that._storage.refFromURL(file.fileUrl);
							httpsReference.delete()
							.catch(function(error2) {
								that.getView().byId("createPage").setBusy(false);
								MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
								return ;
							});
						});
					}
					
					that.getView().byId("createPage").setBusy(false);
					createData.date = createData.date.substring(0,8);
					// MessageBox.error(data.msg);
					MessageBox.error("올바르지 않은 값이 있습니다. 확인 후 다시 시도해주십시오.");
				}
			});
		},
		
		_saveFileStorage : function(fileArr){
			var that = this;
			var form = new FormData();
			for(var i=0; i<fileArr.length; i++){
				form.append("jmtFiles", fileArr[i], fileArr[i].name);
			}
				
			$.ajax({
				url :  "/map_rimmy/api/v1/file",
				method : "POST",
			    processData: false,
			    mimeType: "multipart/form-data",  //header에 넣어보기
			    contentType: false,
			    data: form,
			    timeout: 60000,    //10초 -> 60초
				success: function(data){
					that._saveCreateData(data);
				},
				error: function(error){
					// if(error.status === "timeout"){
					// 	MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
					// }
					MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
					that.getView().byId("createPage").setBusy(false);
				}
				
			});	
		},
		
		onCreateCancel : function(event){
			var that = this;
			var oView = this.getView();
			var createData = JSON.stringify(oView.getModel("createData").getData());
			var originCreateData = JSON.stringify(oView.getModel("originCreateData").getData());
			
			if(createData === originCreateData){
				this.onNavBack();
			}else{
				MessageBox.confirm("확인을 누르시면 작성중이던 데이터는 저장되지 않습니다. 그래도 뒤로 가시겠습니까?",{
					title: "확인",
					onClose : function(action){
						if(action === "OK"){
							// that.resetCreateData();
							that.onNavBack();
							
						}
					}
				});
			}
		},
		
		
		/*
		*	팝업
		*
		*/
		//주소 검색 버큰 클릭, 팝업 생성
		onSearchPop : function(){
			var placeName = this.getView().byId("placeName").getValue();
			if(!placeName || placeName.replace(/ /gi, "").length < 2){
				this.getView().byId("placeName").setValue("");
				MessageBox.error("검색어를 2글자 이상 입력해주세요.");
				return;
			}
			
			var oView = this.getView();
			if (!this.byId("addrDialog")) {
				Fragment.load({
					name: "rimmy.kakao.map.view.AddrPop",
					controller: this,
					id : oView.getId()
				}).then(function(oDialog){
					oView.addDependent(oDialog);
					this.byId("addrDialog").setTitle("주소 검색");
					this.byId("keywordInput").setValue(placeName);
					this.onPopSearchPress();
					oDialog.open();
				}.bind(this));
			} else{
				this.byId("keywordInput").setValue(placeName);
				this.onPopSearchPress();
				this.byId("addrDialog").open();
			}
		},

		onPopSearchPress : function(){
			this.getMapPopList();
		},
		
		//팝업 내에서 검색했을때
		getMapPopList : function(){ 
			var that = this;
			var popItemList = this.byId("popItemList");
			var keywordInput = this.byId("keywordInput").getValue();
			
			if(!keywordInput || keywordInput.replace(/ /gi, "").length < 2){
				this.getView().byId("keywordInput").setValue("");
				MessageBox.error("검색어를 2글자 이상 입력해주세요.");
				return;
			}
			
			popItemList.setBusy(true);
			$.ajax({
				url : "/map_rimmy/api/v1/place",
				method : "POST",
				headers: {
					"Content-Type": "application/json; charset=utf-8"
				},
				data: JSON.stringify({
					"placeName": keywordInput
				}),
				success : function(data,headers){ 
					if (data.status === "error"){ return MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요."); }
					
					if(data.data.metadata.total_count === 0) {//데이터가 없으면
						if(that.getView().getModel("mappoplist")){
							that.getView().getModel("mappoplist").setData("");
						}
						that.byId("popItemList").setNoDataText("검색된 데이터가 없습니다.");
						that.makeMapPopup(127.11674377596071 , 37.484177185974836, null);
						
					}else{//데이터가 있으면
						//하이라이트 제거
						var popListItem = that.getView().byId("popItemList").getAggregation("items");
						for(var i=0; i<popListItem.length;i++){
							if(popListItem[i].getHighlight()!=="None"){
								popListItem[i].setHighlight("None");
								break;
							}
						}
					
						//데이터 바인딩
						if(!that.getView().getModel("mappoplist")){
							var mapModel  = new JSONModel(data.data);
							that.getView().setModel(mapModel, "mappoplist");
						}else{
							that.getView().getModel("mappoplist").setData(data.data);
						}
						var mapPopXyData = that.getView().getModel("mappoplist").getData().mapdata[0];
						that.getView().byId("popItemList").getAggregation("items")[0].setHighlight("Information");
						that.makeMapPopup(mapPopXyData.map.x , mapPopXyData.map.y, mapPopXyData.place_name);
					}
					popItemList.setBusy(false);
				},
				error : function(){
					popItemList.setBusy(false);
					MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
				}
			});
		},
		
		
		//팝업지도
		makeMapPopup: function(x,y,pName){
			var positionPop  = new kakao.maps.LatLng(y, x); 
			this.byId("mapInfoPop").setText(pName);
			
			if(!this.mapPop){
				var divId = this.byId("mapPopup").getId();
				var container = document.getElementById(divId);
				var options = {
					center: positionPop,
					level: 3 
				};
				this.mapPop = new kakao.maps.Map(container, options);
				this.markerPop = new kakao.maps.Marker({
					map : this.mapPop,
				    position: positionPop
				});
				this.markerPop.setMap(this.mapPop);
				var infoPopId = this.byId("mapInfoPop").getId();
				var infoPopContainer = document.getElementById(infoPopId);
				this.infowindow = new kakao.maps.InfoWindow({
				    map: this.mapPop, // 인포윈도우가 표시될 지도
				    position : positionPop, 
				    content : infoPopContainer
				});
				this.infowindow.open(this.mapPop, this.markerPop); 
				if(!pName){
					this.infowindow.close();
				}
					
			}else{
				this.mapPop.setLevel(3);
				this.mapPop.setCenter(positionPop);
				this.markerPop.setPosition(positionPop);
				this.infowindow.open(this.mapPop, this.markerPop); 
				this.infowindow.setPosition(positionPop);
				if(!pName){
					this.infowindow.close();
				}
			}
			
		},
		
		onPopItemPress : function(oEvent){
			var selectedItemIdx = oEvent.getSource().getBindingContextPath().split("/").reverse()[0];
			this.pressIdx = selectedItemIdx;
			//클릭된 값 highlight 바꾸기
			var popListItem = oEvent.getSource().getParent().getAggregation("items");
			for(var i=0; i<popListItem.length;i++){
				if(popListItem[i].getHighlight()!=="None"){
					popListItem[i].setHighlight("None");
					break;
				}
			}
			oEvent.getSource().setHighlight("Information");
			
			var mapData = this.getView().getModel("mappoplist").getData().mapdata[selectedItemIdx];
			this.makeMapPopup(mapData.map.x, mapData.map.y, mapData.place_name);
			
			
		},
		
		//팝업 확인	
		onPopupOK: function(oEvent) { //선택된 값 물고 내려오기
			var selectedIdx = this.pressIdx ;
			if(!selectedIdx){
				selectedIdx = 0;
			}
			var seletedItem = this.getView().getModel("mappoplist").getData().mapdata[selectedIdx];
			this.getView().byId("placeNickName").setValue(this.byId("keywordInput").getValue());
            this.getView().byId("newAddr").setValue(seletedItem.road_addr);
            this.getView().byId("placeName").setValue(seletedItem.place_name);
        	this.getView().getModel("createData").getData().xyData = [seletedItem.map.y, seletedItem.map.x];
            this.createPageMakeMap(seletedItem.map.x, seletedItem.map.y, false, seletedItem.place_name); //지도를 다시 로드
            this.getView().getModel("mappoplist").setData(""); //이전 검색 결과 지우기
            this.byId("mapInfoPop").setText("");
            
            this.pressIdx = null ; 
            this.onRequiredChange();
			this.mapPop = null;
			this.byId("addrDialog").close();
		},

		//팝업 취소
		onPopupCancel: function(oEvent) {
			var mapPopList = this.getView().getModel("mappoplist");
			if(mapPopList){
				this.getView().getModel("mappoplist").setData("");
			}
			this.byId("mapInfoPop").setText("");
			this.mapPop = null;
			this.byId("addrDialog").close();
		}
		
		
		
		
	});
});