sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment",
	"sap/m/MessageToast"
], function (BaseController, JSONModel, MessageBox, Fragment, MessageToast) {
	"use strict";

	return BaseController.extend("rimmy.kakao.map.controller.Detail", {

		onInit: function () {
			var router = this.getRouter();
			router.getRoute("Detail").attachMatched(this._onRouteMatched, this);
		},
		
		_onRouteMatched : function (event) { 
			this._initializationDetail();
			var oArgs = event.getParameter("arguments"); //넘어온 데이터
			
			if(this.getOwnerComponent().mainList){
				var detailData = this.getOwnerComponent().mainList[oArgs.detailIdx];
				if(!detailData){
					this.getRouter().navTo("NotFound");
					return;
				}
				detailData.priceText = detailData.price; 
				detailData.price = Number(detailData.price); 
				this.getView().getModel("originData").setData(JSON.parse(JSON.stringify(detailData)));  //넘어오자마자 오리진 데이터에 셋 해줌,, 
				detailData.date = detailData.date.substring(0,8); 
				this.getView().getModel("detailData").setData(detailData);
				
				this.detailPageMakeMap(detailData.xyData[1],detailData.xyData[0],false,detailData.placeName);
			}
			else{
				this.getRouter().navTo("NotFound");
			}
			
		},
		
		onAfterRendering : function(){
			this._getDetailSelectboxData();
		},
		
		_initializationDetail : function(){
			this.getView().setModel(new JSONModel({}), "detailData");
        	this.getView().setModel(new JSONModel({}), "originData");
        	this.getView().setModel(new JSONModel({
        		"isPossible"		: false,
        		"isShowFooter"		: false,
        		"isEditBtn" 		: true,
        		"isEditBtnEnabled"	: true,
        		"isDeleteBtn"		: true
        	}), "layoutData");
        	
		},
		
		/**
		*
		*상세화면 관련 
		*
		*/
		_getDetailSelectboxData : function(){
			var that = this;
			$.ajax({
				url : "/map_rimmy/api/v1/main/code",
				method : "GET",
				async : false,
				success : function(data){ 
					that.getView().setModel(new JSONModel(data.region), "region"); 
					that.getView().setModel(new JSONModel(data.foodkind), "foodkind"); 
				},
				error : function(error){
					MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
				}
			});
		},
		
		
		
		_modeChange : function(mode){
			var oView = this.getView();
			var oModel = oView.getModel("layoutData");
			oModel.setData({
				isPossible : mode,
				isShowFooter : mode,
				isEditBtnEnabled : !mode,
				isDeleteBtnEnabled : !mode,
				listMode : mode ? "Delete" : "None"
			});
		},
		
		onSelectChange : function(event){
			var detailData = this.getView().getModel("detailData").getData();
			var selectedName = event.getParameter("selectedItem").getText();
			//카테고리가 클릭되면
			if(event.getParameter("id").split("-").reverse()[0] === "foodkind"){
				detailData.foodkindName = selectedName;
			}else{
				detailData.regionName = selectedName;
			}	
		},
		
		onEditBtn : function(event){
			this._modeChange(true);
		},
		
		onSave : function(){
			var that = this;
			var detailData = JSON.parse(JSON.stringify(this.getView().getModel("detailData").getData()));
			var originData = this.getView().getModel("originData").getData();
			
			var saveDate = "";
			if(detailData.date.substring(0,8) === this.getView().getModel("originData").getData().date.substring(0,8)){
				saveDate = this.getView().getModel("originData").getData().date ;
			}
			detailData.date = saveDate;
			if(JSON.stringify(detailData) === JSON.stringify(originData)){
				MessageBox.alert("수정된 데이터가 없습니다.",{
					title: "알림",
					onClose : function(action){
						if(action === "OK"){
							that.getView().byId("detailPage").setBusy(false);
							
						}
					}
				});
				return;
			}
			
			var fileList = this.getView().getModel("detailData").getData().fileArr;
			var newFiles = fileList.filter(function(item){
				return item instanceof Blob;
			});
			MessageBox.confirm("저장하시겠습니까?", {
				title : "저장",
				onClose : function(action){
					if(action === "OK"){
						that.getView().byId("detailPage").setBusy(true);
						if(newFiles.length > 0){
							that._saveFileStorage(newFiles);	
						}else{
							that._saveDetailData();	
						}
					}// OK END
				}
			});  //MessageBox END
		},
		
		
		_saveDetailData : function(filedata){
			var that = this;
			var detailData = this.getView().getModel("detailData").getData();
			// var originData = this.getView().getModel("originData").getData();
			
			var saveDate = "";
			if(detailData.date.substring(0,8) === this.getView().getModel("originData").getData().date.substring(0,8)){
				saveDate = this.getView().getModel("originData").getData().date ;
			}else{
				saveDate = detailData.date + this.getCurrentTime();
			}
			

			//기존에 있었지만 수정하면서 지운 파일을 storage에서 지우기
			if(detailData.fileDeleteList){
				detailData.fileDeleteList.forEach(function(file){
					var httpsReference = that._storage.refFromURL(file.fileUrl);
					httpsReference.delete()
					.catch(function(error) {
						that.getView().byId("detailPage").setBusy(false);
						// MessageBox.error(i18n>error.serviceNotWroking);
						MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
						return ;
					});
				});		
			}

			//신규 추가 파일 처리
			if(filedata){
				var fileData = JSON.parse(filedata).data;
				detailData.fileArr.splice(-fileData.length);
				fileData.forEach(function(item){
					detailData.fileArr.push(item);
				});
			}
			
			//데이터 보수
			detailData.price = detailData.price.toString();
			detailData.delFlag = "";
			detailData.date = saveDate;
			delete detailData.ymdDate ; 
			delete detailData.priceText ; 
			delete detailData.fileDeleteList ; 
			
			//데이터 전송
			$.ajax({
				url :  "/map_rimmy/api/v1/martang",
				method : "PUT",
				headers: {
					"Content-Type": "application/json; charset=utf-8"
				},
				data: JSON.stringify(detailData),
				success : function(data, headers){ 
					if(data.status === "Success"){
						MessageBox.success(data.msg, {
						    title: "확인",
						    onClose: function(action){
						    	if(action === "OK"){
						    		that._modeChange(false);
						    		var detailData2 = JSON.parse(JSON.stringify(that.getView().getModel("detailData").getData()));
						    		that.getView().getModel("originData").setData(detailData2);
						    		that.getView().getModel("detailData").getData().date = that.getView().getModel("detailData").getData().date.substring(0,8);
						    		that.getOwnerComponent().reload = true;
						    	}
						    }
						});
						that.getView().byId("detailPage").setBusy(false);
					}
				},
				error : function(error){ 
					//저장하다가 에러나면 새롭게 추가된 데이터를 지워야됨
					// detailData.fileArr.forEach(function(file){
					// 	var httpsReference = that._storage.refFromURL(file.fileUrl);
					// 	httpsReference.delete()
					// 	.catch(function(error2) {
					// 		that.getView().byId("detailPage").setBusy(false);
					// 		MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
					// 		return ;
					// 	});
					// });	
					
					that.getView().byId("detailPage").setBusy(false);
					MessageBox.error(error.responseText);
				}
			});
		
			
		},
		
		_saveFileStorage : function(newFiles){
			var that = this;
			var form = new FormData();
			for(var i=0; i<newFiles.length; i++){
				form.append("jmtFiles", newFiles[i], newFiles[i].name);
			}
				
			$.ajax({
				url :  "/map_rimmy/api/v1/file",
				method : "POST",
			    processData: false,
			    mimeType: "multipart/form-data",
			    contentType: false,
			    data: form,
				success: function(data){
					that._saveDetailData(data);
				},
				error: function(data){
					MessageBox.error(data.data);
					that.getView().byId("detailPage").setBusy(false);
				}
				
			});	
		},
		
		onDetailBack : function(event){
			var that = this;
			var oView = this.getView();
			var detailData = JSON.parse(JSON.stringify(oView.getModel("detailData").getData()));
			detailData.date = detailData.date.substring(0,8); 
			detailData = JSON.stringify(detailData);
			
			var originDetailData = JSON.parse(JSON.stringify(oView.getModel("originData").getData()));
			originDetailData.date = originDetailData.date.substring(0,8);
			var originData = JSON.stringify(originDetailData);
			
			if(detailData === originData){
				this.onNavBack();
			}else{
				MessageBox.confirm("작성중이던 데이터가 삭제됩니다. 그래도 나가시겠습니까?",{
					title : "뒤로",
					onClose : function(action){
						if(action === "OK"){
							that._modeChange(false);
							originData = JSON.parse(originData);
							originData.date = originData.date.substring(0,8);
							oView.getModel("detailData").setData(originData);
							that.onNavBack();
						}
					}
				});
				// this._mapControll();
			}
		},
		
		onDetailCancel : function(event){
			var that = this;
			var oView = this.getView();
			var detailData = JSON.stringify(oView.getModel("detailData").getData());
			var originDetailData = JSON.parse(JSON.stringify(oView.getModel("originData").getData()));
			originDetailData.date = originDetailData.date.substring(0,8);
			var originData = JSON.stringify(originDetailData);
			if(detailData === originData){
				this._modeChange(false);
				// this.onNavBack();
			}else{
				MessageBox.confirm("작성중이던 데이터가 삭제됩니다. 그래도 취소하시겠습니까?",{
					title : "취소",
					onClose : function(action){
						if(action === "OK"){
							that._modeChange(false);
							originData = JSON.parse(originData);
							originData.date = originData.date.substring(0,8);
							oView.getModel("detailData").setData(originData);
						}
					}
				});
				// this._mapControll();
			}
		},
		
		_getOriginDate : function(oView){
			var originData = oView.getModel("originData").getData();
			originData.date.substring(0,8);
			return originData;
		},
		
		onDeleteBtn : function(event){
			var that = this;
			var detailData = this.getView().getModel("detailData").getData();
			
			MessageBox.confirm("삭제하시겠습니까?",{
				title : "삭제",
				onClose : function(action){
					if(action === "OK"){
						that.getView().byId("detailPage").setBusy(true);
						// debugger;
						if(detailData.fileArr){
							detailData.fileArr.forEach(function(file){
								var httpsReference = that._storage.refFromURL(file.fileUrl);
								httpsReference.delete()
								.catch(function(error) {
									that.getView().byId("detailPage").setBusy(false);
									MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
									return ;
								});
							});
						}
						
						$.ajax({
							url :  "/map_rimmy/api/v1/martang",
							method : "DELETE",
							headers: {
								"Content-Type": "application/json; charset=utf-8"
							},
							data: JSON.stringify({
								"docId"		:	detailData.docId,
								"delFlag"	:   "X"
								
							}),
							success : function(data,headers){
								if(data.status === "Success"){
									MessageBox.success("삭제되었습니다.",{
										title : "삭제",
										onClose : function(action2){
											if(action2 === "OK"){
												that.getOwnerComponent().reload = true;
										    	that.getRouter().navTo("Main");
											}
										}
									});
								}
								that.getView().byId("detailPage").setBusy(false);
							},
							error : function(error){
								that.getView().byId("detailPage").setBusy(false);
								MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
							}
						});
					}else{
						that.getView().byId("detailPage").setBusy(false);
					}//action  END
				}//onClose END
			});//Message END
		},
		
		// 상세화면 지도
		detailPageMakeMap : function(x,y,draggable,pName){
			var divId = this.getView().byId("mapDetailContent").getId();
			var container = document.getElementById(divId);
			var Latitude =  x //위도 
			var longitude =  y //경도
			
			this.getView().byId("mapDetailInfo").setText(pName);
			var infoDivId = this.getView().byId("mapDetailInfo").getId();
			var infoContainer = document.getElementById(infoDivId);
			
			var position  = new kakao.maps.LatLng(longitude, Latitude); 
			var options = {
				center: position,
				level: 3 
			};
			
			if(!this.detailMap){
				this.detailMap = new kakao.maps.Map(container, options);
				this.detailMap.setDraggable(draggable);
				this.detailMap.setZoomable(false);
				this.detailMarker = new kakao.maps.Marker({
					map : this.detailMap,
				    position: position
				});
				this.detailMarker.setMap(this.detailMap);
				this.detailInfowindow = new kakao.maps.InfoWindow({
				    position : position, 
				    content : infoContainer
				});
				if(pName){
					this.detailInfowindow.open(this.detailMap, this.detailMarker); 
				}
			}else{
				this.detailMap.setDraggable(draggable);
				this.detailMap.setZoomable(false);
			    this.detailMap.panTo(position);    
			    this.detailMarker.setPosition(position);
		    	this.detailInfowindow.open(this.detailMap, this.detailMarker);
		    	this.detailInfowindow.setPosition(position);
			}
		},
		

		onRequiredChange : function(event){ 
			var detailData = this.getView().getModel("detailData").getData();

			//필수값 체크 
			if(detailData.placeName){
				var placeName = detailData.placeName.replace(/ /gi, "");
			}
			if(detailData.address){
				var newAddr = detailData.address.replace(/ /gi, "");
			}
			var isPriceValue = detailData.price > 0 ? true : false ;

			//조건들을 다 통과하면 저장버튼 활성화
			if( placeName && newAddr && isPriceValue && detailData.rating && detailData.address){
				this.getView().byId("id-Save").setEnabled(true);
			}else{
				this.getView().byId("id-Save").setEnabled(false);
			}
		},
		
		onPriceChange : function(event){ debugger;
			var detailData = this.getView().getModel("detailData").getData();
			var selectedPrice = detailData.price;
			detailData.priceText = selectedPrice;
			
			if(event.getSource().getId().split("-").reverse()[0] === "priceValue"){
				if( event.getSource().getValue() >= 0 &&  event.getSource().getValue() <= 100000){
					event.getSource().setValueState("None");
					event.getSource().setValueStateText("");
					detailData.priceText = event.getSource().getValue().toLocaleString();
					detailData.price = Number(event.getSource().getValue());
				}else{
					event.getSource().setValueState("Error");
					event.getSource().setValueStateText("0이상 100000이하의 값으로 입력해주세요.");
					event.getSource().setValue("");
					detailData.price = 0;
				}
					
			}	
		},
		
		onDateChange: function (event) {
			var oSource = event.getSource();
			if(!event.getParameter("valid") || !event.getParameter("newValue")){ //빈값이면 true로 떨어짐
				oSource.setValueState("Error");
				oSource.setValueStateText("ex: 20200130 과 같은 형식으로 입력해주세요.");
				oSource.setValue("");
				oSource.setPlaceholder("ex: 20200130");
			}else{
				oSource.setValueState("None");
				oSource.setValueStateText("");
			}
		},
		
		
		//파일 리스트 
		onAttachFile : function(event){
			var that = this;
			var fileModel = that.getView().getModel("detailData");
			var fileArr = fileModel.getData().fileArr ;
			
			if(fileArr.length >= 3){
				MessageBox.alert("이미지 첨부는 최대 3개까지 가능합니다.",{
					title: "첨부파일"
				});
				event.getSource().setValue("");
				return;
			}
			
			var fileObject = event.getParameter("files")[0]; //하나씩 가져오니까
			if(fileObject){
				fileArr.push(fileObject);
				fileModel.refresh();
				event.getSource().setValue("");
			}
		},
		
		
		onFileDelete : function(event){
			var deletePath = event.getParameter("listItem").getBindingContextPath().split("/").reverse()[0];
			var detailDataModel = this.getView().getModel("detailData");
			var detailData = detailDataModel.getData();
			var fileList = detailData.fileArr;
			// var fileDeleteList = [];
			// fileDeleteList.push(fileList[deletePath]);
			if(!detailData.fileDeleteList){
				detailData.fileDeleteList = [];	
			}
			detailData.fileDeleteList.push(fileList[deletePath]);
			
			
			fileList.splice(deletePath,1);
			detailDataModel.setData(detailData);
			detailDataModel.refresh();
		},
		
		onItemDownload : function(event){
			var that = this;
			var selectedIdx = event.getSource().getBindingContextPath().split('/').reverse()[0];
			var fileList = this.getView().getModel("detailData").getData().fileArr;
			var httpsReference = this._storage.refFromURL(fileList[selectedIdx].fileUrl);
			httpsReference.getDownloadURL()
			.then(function(url) {
				var xhr = new XMLHttpRequest();
				xhr.responseType = 'blob';
				xhr.onload = function(event) {
					var blob = xhr.response;
					var download = document.createElement('a');
					download.href = URL.createObjectURL(blob, { type: blob.type });
					MessageToast.show(`${fileList[selectedIdx].name} 다운로드`);
					download.download = fileList[selectedIdx].name;
					download.target = "_blank";
					document.body.appendChild(download);
					download.click();
					document.body.removeChild(download);
				};
				xhr.onerror = function(error){
					// MessageBox.error(that.i18n.getText("ERROR_DOWNLOAD"))
					MessageBox.error("다운로드를 할 수 없습니다. 관리자에게 문의바랍니다.")
				}
				xhr.open('GET', url);
				xhr.send();
			}).catch(function(error) {
			// Handle any errors
			});
		}
	});

});