sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"sap/m/MessageToast"
], function (Controller, MessageBox, MessageToast) {
	"use strict";

	return Controller.extend("com.istn.ci.FileDownload.FileDownload.controller.Main", {
		gySampleFile: '/jmt/MS-Azure.png',
		onInit: function () {
			
		},
		
		onAfterRendering: function(){
			this.storage = this.getOwnerComponent().storage;
			this.byId('idStorargePath').setPlaceholder(this.i18n.getText("FILEPATH_PLACEHOLDER"));
		},
		
		onDownloadFile: function(event){
			var filePath = this.byId("idStorargePath").getValue();
			if(!filePath) return MessageBox.error(this.i18n.getText("REQUIRED_FILEPATH"));
			
			if(!this.storage) return MessageBox.error(this.i18n.getText("NO_STORAGE_OBJECT"));
		
			var fileRef = this.storage.ref(), that = this;
			fileRef.child(filePath).getDownloadURL()
				.then(function(url) {
					MessageToast.show(that.i18n.getText("START_DOWNLOAD"));
					that._downLocalSystem(url);
				})
				.catch(function(error) {
					MessageBox.error(that.i18n.getText("YOUR_MESSAGE"))
				});
		},
		
		_downLocalSystem: function(url){
			var xhr = new XMLHttpRequest(), that = this;
			xhr.responseType = 'blob';
			xhr.onload = function(event) {
				var blob = xhr.response;
				var download = document.createElement('a');
				download.href = URL.createObjectURL(blob, { type: blob.type });
				that.byId("idPreviewImage").setSrc(download.href);
				download.download = `${that.byId('idStorargePath').getValue()}`;
				download.target = "_blank";
				document.body.appendChild(download);
				download.click();
				document.body.removeChild(download);
			};
			xhr.onerror = function(error){
				MessageBox.error(that.i18n.getText("ERROR_DOWNLOAD"))
			}
			xhr.open('GET', url);
			xhr.send();
		}
	});
});