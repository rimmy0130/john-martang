sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/core/UIComponent",
	"sap/m/MessageBox",
	"sap/ui/model/json/JSONModel"
], function(Controller, History, UIComponent, MessageBox, JSONModel) {
	"use strict";

	return Controller.extend("rimmy.kakao.map.controller.BaseController", {
		onBeforeRendering : function(){
			this._initFirebase();
		},
		
		getRouter : function () {
			return UIComponent.getRouterFor(this);
		},

		onNavBack: function () { 
			var oHistory, sPreviousHash;

			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("Main", {}, true /*no history*/);
			}
		},
		
		_getSavedDataList : function(){  
			var that = this;
			var savedListItem = this.getView().byId("saveditemlist");
			
			savedListItem.setBusy(true);
			$.ajax({
				url : "/map_rimmy/api/v1/martang",
				method : "GET",
				success : function(data,headers){ 
					that.getOwnerComponent().mainList = data;  //사용자가 저장한 모든 데이터
					if (data.status === "error"){ return MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요."); }
					
					if(data.length === 0) {//데이터가 없다면
						if(that.getView().getModel("savedMaplist")){
							that.getView().getModel("savedMaplist").setData(data);
						}
						that.getView().byId("saveditemlist").setNoDataText("나만의 맛집이 없습니다. 신규 등록을 먼저 해주세요.");
						that.makeMap(127.11674377596071 , 37.484177185974836, null, true); //시작점
					}else{//데이터가 있다면	
						//데이터 바인딩
						if(!that.getView().getModel("savedMaplist")){
							var savedMapModel  = new JSONModel(data);
							that.getView().setModel(savedMapModel, "savedMaplist");
						}else{
							that.getView().getModel("savedMaplist").setData(data);
						}
						
						that.mainOriginData = that.getView().getModel("savedMaplist").getData();
						var mapXyData = that.getView().getModel("savedMaplist").getData()[0];
						that.getView().byId("saveditemlist").getAggregation("items")[0].setHighlight("Information");
						that.makeMap(mapXyData.xyData[1] , mapXyData.xyData[0], mapXyData.placeName, true);
					}
					savedListItem.setBusy(false);
				},
				error : function(){
					savedListItem.setBusy(false);
					MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
				}
			});
		},
		
		//오늘 날짜 계산
		getTodayDate : function(){
			var date = new Date();
			var year = date.getFullYear();
			var month = date.getMonth() + 1;
			var day = date.getDate();
			
			if (month < 10) {
				month = "0" + month;
			}else{
				month = "" + month;
			}
			
			if (day < 10) {
				day = "0" + day;
			}
			return year+month+day ;
		},
		
		getCurrentTime : function(){
			var date = new Date();
			var hour = date.getHours();
			var minute = date.getMinutes();
			var second = date.getSeconds();
			
			// 한자리수일 경우 0을 채워준다. 
			if (hour < 10) {
				hour = "0" + hour;
			}else{
				hour = "" + hour;
			}
			if (minute < 10) {
				minute = "0" + minute;
			}
			if (second < 10) {
				second = "0" + second;
			}
			
			return hour + minute + second;
		},
	
		//생성하지 않고 취소/뒤로가기 눌렀을때 생성하려던 데이터 reset
		resetCreateData : function(){
			this.getView().byId("placeName").setValue("");
			this.getView().byId("newAddr").setValue("");
			this.getView().byId("region").setSelectedKey(""); //getValue
			this.getView().byId("placeNickName").setValue("");
			this.getView().byId("priceSlider").setValue(0);
			this.getView().byId("priceValue").setValue("");
			this.getView().byId("foodkind").setSelectedKey("");
			this.getView().byId("recoMan").setValue("");
			this.getView().byId("rating").setValue(3);
			this.getView().byId("comment").setValue("");
			this.getView().byId("ateDate").setValue(this.getTodayDate().substring(0,8));
			this.getView().byId("ateDate").setValueState("None");
		},
		
		_initFirebase: function(){ 
			var firebaseConfig = {
			    apiKey: "AIzaSyDOFcRs9ypLOBkSoRsSL4fNQeWKZeFaXtU",
			    authDomain: "john-martang.firebaseapp.com",
			    databaseURL: "https://john-martang.firebaseio.com",
			    projectId: "john-martang",
			    storageBucket: "john-martang.appspot.com",
			    messagingSenderId: "227075007155",
			    appId: "1:227075007155:web:295b27c779fa406a2e46c6",
			    measurementId: "G-WCHTPMQQ4D"
			  };
			  
			//듀플 에러
			if(!firebase.apps.length){
				firebase.initializeApp(firebaseConfig);
			}
			this._storage = firebase.storage();
		},
		
		getRangeDate : function(pDate){
			var year = pDate.getFullYear();
			var month = pDate.getMonth() + 1;
			var day = pDate.getDate();
			
			if (month < 10) {
				month = "0" + month;
			}else{
				month = "" + month;
			}
			
			if (day < 10) {
				day = "0" + day;
			}
			
			return year+month+day ;
			
		}

	});

});