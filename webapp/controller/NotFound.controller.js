sap.ui.define([
	"./BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("rimmy.kakao.map.controller.NotFound", {
		onNotFoundBack : function(){
			this.getRouter().navTo("Main");
			
		}

	});

});