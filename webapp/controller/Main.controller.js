sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment"
], function (BaseController, JSONModel, MessageBox, Fragment) {
	"use strict";

	return BaseController.extend("rimmy.kakao.map.controller.Main", {
		onInit: function () {
			this.router = this.getRouter();
			this.router.getRoute("Main").attachPatternMatched(this._onObjectMatched, this);
			this.getOwnerComponent().reload = true;
		},

		_onObjectMatched : function(oEvent){
			
			if(this.getOwnerComponent().reload){
				this._getSavedDataList();
				this.getOwnerComponent().reload = false;
			}
		},
		
		onAfterRendering : function(){
			this._initializationMain();
		},

		_initializationMain : function() {
			var oView = this.getView();
			this._getMainSelectboxData();
			oView.setModel(new JSONModel({}), "search");  //main search Condition
			oView.getModel("search").setData({ "date" : "", "region" : "", "foodkind" : "" });
		},
		
		_getMainSelectboxData : function(){
			var that = this;
			var regionData ;
			var foodkindData ;
			
			$.ajax({
				url : "/map_rimmy/api/v1/selectbox/all",
				method : "GET",
				async : false,
				success : function(data){ 
					regionData = data.region;
					foodkindData = data.foodkind;
					
					regionData.unshift({
						"regionName" : "전체",
						"regionKey"  : ""
					});
					foodkindData.unshift({
						"foodkindName" : "전체",
						"foodkindKey"  : ""
					});
					
					that.getView().setModel(new JSONModel(regionData), "region"); 
					that.getView().setModel(new JSONModel(foodkindData), "foodkind"); 
						
				},
				error : function(error){
					MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
				}
			});
		},
		
		/**
		*
		*메인화면 관련 
		*
		*/
		//메인화면 열리자마자되는 바인딩
		_getSavedDataList : function(){  
			var that = this;
			var savedListItem = this.getView().byId("saveditemlist");
			savedListItem.setBusy(true);
			$.ajax({
				url : "/map_rimmy/api/v1/martang",
				method : "GET",
				success : function(data,headers){ 
					that.getOwnerComponent().mainList = data;  //사용자가 저장한 모든 데이터
					if (data.status === "error"){ return MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요."); }
					
					if(data.length === 0) {//데이터가 없다면
						if(that.getView().getModel("savedMaplist")){
							that.getView().getModel("savedMaplist").setData(data);
						}
						that.getView().byId("saveditemlist").setNoDataText("나만의 맛집이 없습니다. 신규 등록을 먼저 해주세요.");
						that.makeMap(127.11674377596071 , 37.484177185974836, null, true); //시작점
					}else{//데이터가 있다면	
						//데이터 바인딩
						if(!that.getView().getModel("savedMaplist")){
							var savedMapModel  = new JSONModel(data);
							that.getView().setModel(savedMapModel, "savedMaplist");
						}else{
							that.getView().getModel("savedMaplist").setData(data);
						}
						
						if(!that.getOwnerComponent().reload){
							var mainListItem = that.getView().byId("saveditemlist").getAggregation("items");
							for(var i=0; i<mainListItem.length;i++){
								if(mainListItem[i].getHighlight()!=="None"){
									mainListItem[i].setHighlight("None");
									break;
								}
							}
						}
						
						that.mainOriginData = that.getView().getModel("savedMaplist").getData();
						var mapXyData = that.getView().getModel("savedMaplist").getData()[0];
						that.getView().byId("saveditemlist").getAggregation("items")[0].setHighlight("Information");
						that.makeMap(mapXyData.xyData[1] , mapXyData.xyData[0], mapXyData.placeName, true);
					}
					savedListItem.setBusy(false);
				},
				error : function(error){
					savedListItem.setBusy(false);
					MessageBox.error("현재 서비스가 정상적이지 않습니다. 잠시후 다시 시도해주세요.");
				}
			});
		},
		
		makeMap : function (x,y,pName,draggable) {
			this.getView().byId("mapInfo").setText(pName);
			var position  = new kakao.maps.LatLng(y, x); 
			
			if(!this.mainMap){
				var divId = this.getView().byId("mapContent").getId();
				var container = document.getElementById(divId);
				var infoDivId = this.getView().byId("mapInfo").getId();
				var infoContainer = document.getElementById(infoDivId);
				
				var options = {
					center: position,
					level: 3 
				};
			
				this.mainMap = new kakao.maps.Map(container, options);
				this.mainMap.setDraggable(draggable);
				this.mainMarker = new kakao.maps.Marker({
					map : this.mainMap,
				    position: position
				});
				this.mainMarker.setMap(this.mainMap);
				this.infowindow = new kakao.maps.InfoWindow({
				    position : position, 
				    content : infoContainer
				});
				this.infowindow.open(this.mainMap, this.mainMarker);
				if(!pName){
					this.infowindow.close()
				}
			}else{
				//this.mainMap.setDraggable(draggable);
			    this.mainMap.setLevel(3);
			    this.mainMap.setCenter(position);
			    this.mainMap.panTo(position);    
			    this.mainMarker.setPosition(position);
			    this.infowindow.open(this.mainMap, this.mainMarker);
			    this.infowindow.setPosition(position);
				if(!pName){
					this.infowindow.close();
					
				}
			}
		},
		
		onSwichPress : function(event){ 
			var mainDataList = this.getView().getModel("savedMaplist");
			if(!mainDataList){
				this.getView().byId("idAllMap").setEnabled(false);
				return;
			}else{
				this.getView().byId("idAllMap").setEnabled(!this.getView().byId("idSwitch").getState());
				
			}
		},
		
		onAllMap : function(){
			this.infowindow.close()
			var clusterData = this.getView().getModel("savedMaplist").getData();
			var bounds = new kakao.maps.LatLngBounds();   
			
			var i;
			var marker;
			this.allMarkers = [];
			for (i = 0; i < clusterData.length; i++) {
			    var marker = new kakao.maps.Marker({
			        position: new kakao.maps.LatLng(clusterData[i].xyData[0], clusterData[i].xyData[1])
			    });
			    marker.setMap(this.mainMap);
			    this.allMarkers.push(marker);
			    
			    bounds.extend(new kakao.maps.LatLng(clusterData[i].xyData[0], clusterData[i].xyData[1]));
			}
			
			this.mainMap.setBounds(bounds);
		},
		
		onCreate : function(){
			this.getRouter().navTo("Create");
		},
		
		/**
		*
		*메인 리스트 관련 
		*
		*/
		onListItemPress : function(oEvent){ 
			var selectedItemIdx = oEvent.getSource().getBindingContextPath().split("/").reverse()[0];

			//클릭된 값 highlight 바꾸기
			var mainListItem = oEvent.getSource().getParent().getAggregation("items");
			for(var i=0; i<mainListItem.length;i++){
				if(mainListItem[i].getHighlight()!=="None"){
					mainListItem[i].setHighlight("None");
					break;
				}
			}
			
			if(this.getView().byId("idSwitch").getState()){
				mainListItem[0].setHighlight("Information");
				//지도 바꾸기
				var xyData = this.getView().getModel("savedMaplist").getData()[0].xyData ;
				var pName = this.getView().getModel("savedMaplist").getData()[0].placeName;
				this.makeMap(xyData[1], xyData[0], pName ,true);
				
				this.getRouter().navTo("Detail",{ //디테일로 보내기
					detailIdx: selectedItemIdx
				});
			}else{
				oEvent.getSource().setHighlight("Information");
				
				if(this.allMarkers){
					for(var j=0;j<this.allMarkers.length;j++){
						this.allMarkers[j].setMap(null); 
					}
				}
				
				//지도 바꾸기
				var xyData = this.getView().getModel("savedMaplist").getData()[selectedItemIdx].xyData ;
				var pName = this.getView().getModel("savedMaplist").getData()[selectedItemIdx].placeName;
				this.makeMap(xyData[1], xyData[0], pName ,true);
			}
		},

		/* 메인 필터 관련 */
		//검색 조건 초기화
		onReset : function(event){
			this.getView().getModel("search").setData({ "date" : "", "region" : "", "foodkind" : "" });
			this.onMainSearchChange(event);
		},
		
		//메인 달력 조건
		onMainSearchChange : function(event){
			var originData = this.mainOriginData;
			var searchCondition = this.getView().getModel("search").getData();
			
			//highlight 초기화
			var mainListItem = this.getView().byId("saveditemlist").getAggregation("items");
			for(var i=0; i<mainListItem.length;i++){
				if(mainListItem[i].getHighlight()!=="None"){
					mainListItem[i].setHighlight("None");
					break;
				}
			}
			//datePicker Validation 
			if(event.getSource().getId().split("-").reverse()[0] === "dateSelect" ){
				if(!event.getParameter("valid")){
					event.getSource().setValueState("Error");
					event.getSource().setValueStateText("ex: 202008 과 같은 형식으로 입력해주세요.");
					return;
				}
				
				if(event.getSource().getValueState() === "Error"){
					event.getSource().setValueState("None");
					event.getSource().setValueStateText("");
				}	
			}
			//Search
			var changeData = originData.filter(function(dateItem){
				if(dateItem.date.substring(0,6) === searchCondition.date){
					return dateItem;
				}
				else if(searchCondition.date === "" || !searchCondition.date){
					return dateItem;
				}
			})
			.filter(function(regionItem){
				if(regionItem.region === searchCondition.region){
					return regionItem;
				}
				else if(searchCondition.region === "" || !searchCondition.region){
					return regionItem;
				}
			})
			.filter(function(foodkindItem){
				if(foodkindItem.foodkind === searchCondition.foodkind){
					return foodkindItem;
				}
				else if(searchCondition.foodkind === "" || !searchCondition.foodkind){
					return foodkindItem;
				}
				
			});
			//Search Result Bind
			this.getView().getModel("savedMaplist").setData(changeData);
			
			if(changeData.length === 0){
				this.getView().byId("saveditemlist").setNoDataText("조건에 맞는 검색 결과가 없습니다.");
				return;
			}else{
				this.getOwnerComponent().mainList = changeData;
				//지도 바꾸기
				var xyData = this.getView().getModel("savedMaplist").getData()[0].xyData ;
				var pName = this.getView().getModel("savedMaplist").getData()[0].placeName;
				this.makeMap(xyData[1], xyData[0], pName ,true);
				
				this.getView().byId("saveditemlist").getAggregation("items")[0].setHighlight("Information");
			}
		}
	});
});